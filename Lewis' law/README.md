# Supplementary Material

## [Code_LBIBCell](https://git.bsse.ethz.ch/iber/Publications/2021_kokic_vetter_epithelial_organisation/tree/master/Code_LBIBCell)
* Here you find the BioSolvers that were created in addition to [LBIBCell](https://tanakas.bitbucket.io/lbibcell/index.html)'s default BioSolvers. 
## [Code_R](https://git.bsse.ethz.ch/iber/Publications/2021_kokic_vetter_epithelial_organisation/tree/master/Code_R)
* This script processes XLS files that were created by the [ICY](http://icy.bioimageanalysis.org) plugin [EpiTools](https://epitools.github.io).

## [Experimental_Data](https://git.bsse.ethz.ch/iber/Publications/2021_kokic_vetter_epithelial_organisation/tree/master/Experimental_Data)
* This folder contains all data on the following tissues:
    * dPE (*Drosophila* eye disc peripodial membrane), 
    * dWL (*Drosophila* larval wing disc), 
    * dPW (*Drosophila* pupal wing disc) and 
    * dMWL (*Drosophila* mutant "gigas" larval wing disc).