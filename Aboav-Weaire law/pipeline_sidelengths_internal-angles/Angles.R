## Purpose of script: Extract internal angles of segmented polygons 
## from SQL database created by Tissue Analyzer FIJI plugin
##
## Copyright (c) Marco Kokic, 2019
## Email: marco.kokic@bsse.ethz.ch


# Load R Database Interface package
library(DBI) 
# Set working directory (folder where database is stored) 
setwd(yourfolder)
# Read DB tables and merge extracted queries 
con <- dbConnect(RSQLite::SQLite(), "./TA.db")
Bonds = dbReadTable(con, "Bonds")
Cells = dbGetQuery( con,
                    'select area_cells, 
                    local_id_of_bonds,
                    nb_of_vertices_no_cut_off,
                    local_id_cells from Cells where is_border_cell_plus_one="false"') # exclude border cells
library(stringr) 
Cells$len <- sapply(gregexpr("\\#", Cells$local_id_of_bonds), length) + 1
Cells = Cells[Cells$len<15,]
Cells <- data.frame(cbind(Cells,str_split_fixed(Cells$local_id_of_bonds, "#", 
                                                max(Cells$len))))
indx <- sapply(Cells, is.factor)
Cells[indx] <- lapply(Cells[indx], function(x) as.integer(as.character(x)))
bc <- c()
for (vertices in 1:max(Cells$nb_of_vertices_no_cut_off)) {
  bc[[vertices]] <-  merge(Bonds,Cells,by.x="local_id_bonds",
                           by.y=paste0("X",vertices))
}
# Now extract each cell's unique vertices
library(plyr)
bc = rbind.fill(bc)
bc1 <- data.frame(x=bc$vx_1_x,
                  y=bc$vx_1_y,
                  cellID=bc$local_id_cells,
                  Area=bc$area_cells)
bc2 <- data.frame(x=bc$vx_2_x,
                  y=bc$vx_2_y,
                  cellID=bc$local_id_cells,
                  Area=bc$area_cells)
bc <- rbind(bc1,bc2)
bc <- bc[!duplicated(bc), ]
bc_unique <- ddply(bc, .(cellID),head,n=1) 
Area <- bc_unique$Area
# Now, we have to sort the vertices in a clockwise fashion
# and then calculate the internal angles in a 3-point-wise fashion
getangles <- function(polygon) {
  # compute vertex points using convex hull algorithm 
  # (makes polygon coarser, e.g. 1px sides )
  polygon <- polygon[chull(polygon[,1:2]),] 
  # now consider groups of 3 neighbouring vertices
  # sequence of middle vertices
  indv2 <- seq_len(nrow(polygon))
  # sequence of right and left neighbouring vertices 
  indv1 <- c(indv2[-1], indv2[1])
  indv3 <- c(indv2[length(indv2)], indv2[-length(indv2)])
  # The following reads: angle(ray 1, x-axis)-angle(ray 2, x-axis) = internal angle
  ((atan2(polygon$y[indv1] - polygon$y[indv2], polygon$x[indv1] - polygon$x[indv2]) -
      atan2(polygon$y[indv3] - polygon$y[indv2], polygon$x[indv3] - polygon$x[indv2])) * 180 / pi) %% 360
} 
# Calculate all internal angles of all cells
allangles <- by(bc, bc$cellID, getangles)

# Convert list of atomic vectors to dataframe and prep it
DF <- ldply(allangles, rbind)
cellID <- DF$.id
DF$.id=NULL # Must be removed to calculate correct polygonNo
DF[is.na(DF)]<-0
DF$polygonNo <- rowSums(DF != 0)
DF$cellID =cellID
DF$Area = Area
DF <- DF[DF$polygonNo!=0,]
library(reshape2)
names <- colnames(DF)
# 1 : max number of internal angles in dataframe
names = names[1:(length(names)-3)]
# get molten dataframe (stacks angle columns into single column)
# ..and do some formatting
output <- melt(DF, id.vars=c("polygonNo","cellID"),measure.vars=c(names))
output <- mapply(output,FUN= as.numeric) # convert to numeric
output <- as.data.frame(output)
# exclude nonexistent angles
output = subset(output,output$value!=0)
names(output)=c("PolygonNo","cellID","variable","angle")
output$variable=NULL
# combine angles and areas via cell ID
DF =DF[,c("cellID","Area")]
output = merge(output,DF,by = "cellID")

# export data
write.csv(d,paste0(getwd(),'_angles.csv'),row.names=FALSE)

