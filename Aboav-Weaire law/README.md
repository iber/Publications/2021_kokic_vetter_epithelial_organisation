# Supplementary Material

## EpiTools_modification 
* Here you find the modified JAR file that belongs to the *Icy* plugin *EpiTools*. 
* The modification adds the IDs of the n<sub>i</sub> neighbours of a cell as an additional column in the XLS output file created by EpiTools' *CellExport* function.
* The output file can be further processed using the scripts in the subsequent pipeline folders.
* Installation instruction: Just replace this file in the plugin folder. 
* Installation instructions for the *Icy* plugin *EpiTools* can be found [here](https://epitools.github.io/wiki/Icy_Plugins/00_Installation/).
 
## pipeline_m_n
* This folder contains an R script with which you can extract the average number of neighbours m<sub>n</sub> of the n<sub>i</sub> neighbours of a cell n.
* As an input you need the XLS output file that you created with the modified Icy plugin *EpiTools*.

## pipeline_sidelengths_internal-angles
* This folder contains R scripts with which you can extract the internal angles and sidelengths of segmented cells from an SQL database *TA.db* created by the Fiji plugin *TissueAnalyzer*.
* Installation instructions for *TissueAnalyzer* can be found [here](https://github.com/mpicbg-scicomp/tissue_miner/blob/master/MovieProcessing.md).
* In *TissueAnalyzer*, the database *TA.db* is created in the following way:
    - As an input, you need a 2D microscopy image of visible cell boundaries, ideally a binary image (white boundaries, black background).
    - *TissueAnalyzer* can be used to create such a segmentation using the *Detect Bonds* functions in the *Segmentation* tab of the plugin. 
    - Manual curation of the segmentation is a valuable feature of the plugin and is highly recommended.
    - Assuming you have such a curated segmentation, you can create the database by selecting *Finish all* in the tab *PostProcess*.

## supplement_analytical_approximation
* This folder contains MATLAB scrips and functions that allow to evaluate and fit the theoretical approximation to Aboav-Weaire's law developed in the Supplementary Material. 
* The fitting functionality requires the `fitnlm` function from MATLAB's Statistics Toolbox.

