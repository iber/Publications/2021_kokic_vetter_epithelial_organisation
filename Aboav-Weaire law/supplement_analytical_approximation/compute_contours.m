% This script calculates the two sets of surface contours shown in the
% Supplementary Material which cannot be expressed as an explicit function
% during plotting, plots them, and writes them to raw text files.

afunc = @(x,y) 180/(y-x)*log((180-x)/(180-y)*y/x);
bfunc = @(x,y) 2*(180/x)*(180/y);

z = [4.6 4.8 5]; % contour levels
x0 = [36 30 26];
y0 = [145 150 155];

figure
hold all
box on
axis equal
xlim([0 90])
ylim([90 180])

d = [];
for k = 1:length(z)
    x = x0(k):1:90;
    y = y0(k)*ones(size(x));
    for i = 1:length(x)
        y(i) = fzero(@(y) afunc(x(i),y)-z(k), y(max(1,i-1)));
    end
    plot(x, y, 180-y, 180-x)
    xlabel('\theta_{min}')
    ylabel('\theta_{max}')
    d = [x', y'];
    save(['contour_a' num2str(z(k)) '.txt'], 'd', '-ascii')
end

z = [50 60 70]; % contour levels
a0 = [6 6 4.8];
a1 = [4.3 4.15 4.15];
b0 = [7.5 6.7 6];

figure
hold all
box on
axis equal
xlim([4 6])
ylim([6 10])

d = [];
for k = 1:length(z)
    a = a0(k):-0.05:a1(k);
    b = b0(k)*ones(size(a));
    for i = 1:length(a)
        b(i) = fzero(@(b) afunc(z(k),fzero(@(tmax) bfunc(z(k),tmax)-b, 150))-a(i), b(max(1,i-1)));
    end
    plot(a, b)
    xlabel('a')
    ylabel('b')
    d = [a', b'];
    save(['contour_tmin' num2str(z(k)) '.txt'], 'd', '-ascii')
end
